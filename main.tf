terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.37.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "iaac-rg" {
  name     = "iaac-resource-group"
  location = "Central India"

  tags = {
    environment = "iaac"
  }
}
